<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\updateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

use App\Users;



class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getUser(){
        $user=Auth::user();
        $tag=DB::table('country')->get()->toArray();
        return view ('admin.user.pages_profiles',compact('user','tag'));
        }
        public function postupdateUser(updateRequest $request){
          
           $userid=Auth::id();
           $user=Users::findOrFail($userid);
           $data=$request->all();
           $file=$request->avatar;

           if(!empty($file)){
               $data['avatar']=$file->getClientOriginalName();
           }
           if($data['password']){
               $data['password']=bcrypt($data['password']);
           }else{
            $data['password']=$user->password;
           }
           if($user->update($data)){
               if(!empty($file)){
                   $file->move('upload/user/avatar',$file->getClientOriginalName());
        
               }
               return redirect()->back()->with('sucess', ('Update profile sucess'));
    
           }else{
               return redirect()->back()->withErrors("Update profile error");
           }
    
    
        }
        
}
