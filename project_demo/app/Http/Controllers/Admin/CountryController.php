<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CountryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showCountry(){
        $data=DB::table('country')->get()->toArray();
        return view('admin.country.country',compact('data'));
    }
    public function getaddCountry(){
        return view('admin.country.add');
    }
    public function postaddCountry(Request $request){
        DB::table('country')->insert(
            ['id'=>$request->ID,
            'name'=>$request->name,
            ]
        );
        return redirect('country');     
    }
    public function deleteCountry(Request $request,$id){
        DB::table('country')->where('id','=',$id)->delete();
        return redirect('country');  
    }
}
