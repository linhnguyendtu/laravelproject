<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin;

//Route::get('test','DashboardController@test');
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });







// Route::get('/dashboard',function(){
//     return view('admin.dashboard');
// });
// Route::get('/pages_profiles',function(){
//     return view('admin.user.pages_profiles');
// });
Route::get('/form',function(){
    return view('admin.dashboard.form_basic');
});
Route::get('/table',function(){
    return view('admin.dashboard.table');
});
Route::get('/icon',function(){
    return view('admin.dashboard.icon');
});
Route::get('/blank',function(){
    return view('admin.dashboard.blank');
});
Route::get('/404',function(){
    return view('admin.dashboard.404');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::group([
   
    'namespace'=>'Admin',
],function(){

    Route::get('home','DashboardController@getHome')->middleware('checkLogin');


    Route::get('user','UserController@getUser');
    Route::post('user','UserController@postupdateUser');

    Route::get('country','CountryController@showCountry');
    Route::get('add','CountryController@getaddCountry');
    Route::post('add','CountryController@postaddCountry');
    Route::get('delete/{id}','CountryController@deleteCountry');

    




});
