@extends('admin.layouts.master')
@section('content')
<!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Country</h4>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex align-items-center justify-content-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Country</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <table id="datatable" style="border: 1px solid">
                               
                                <thead>
                                    <tr role="row">
                                        <th>ID</th>
                                        <th>Tittle</th>
                                        <th>Image</th>
                                        <th>Description</th>
                                        <th style="width: 7%;">Action</th>
                                        
                                    </tr>
                                </thead>
                                     @foreach($data as $value)
                                        <tr role="row">
                                        <td>{{$value->id}}</td>
                                        <td>{{$value->name}}</td>
                                        <td></td>
                                        <td></td>
                                        <td><a href={{url('delete/'.$value->id)}}>Delete</a></td>
                                       
                                        </tr>
                                    @endforeach
                                    
                                
                                </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="8">
                                                    <a href='add'><button style="background-color: #6ab04c;" id="button">Add Country</button></a>
                                                </td>
                                            </tr>
                                        </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
           
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
@endsection